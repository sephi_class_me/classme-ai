import numpy as np
import tensorflow as tf
import random, re, string, pickle

MIN_WORDS = 2
MAX_WORDS = 4

def save_obj(obj, name ):
    with open(name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_obj(name ):
    with open(name + '.pkl', 'rb') as f:
        return pickle.load(f)

fastTextDict = load_obj("fastTextDic")

def getFastTextDictionary(num = 0):
    global fastTextDict
    if fastTextDict: return fastTextDict

    fastText = {}

    with open("wiki.he.vec", encoding='utf-8') as f:
        lines = f.readlines()
    lines = [x.rstrip() for x in lines]

    for i, wordAndVec in enumerate(lines):
        if num > 0 and i >= num: break
        vec = wordAndVec.split(" ")

        word = vec.pop(0)
        if len(vec) != 300:
            print (word)
            continue
        embeddedVectorFloat = np.array(vec).astype(np.float)
        fastText[word] = embeddedVectorFloat

    fastTextDict = fastText
    return fastTextDict

def getFastTextSentecesAndClasses(files):

    lines = []
    for file in files:
        with open(file, encoding='utf-8') as f:
            lines += f.readlines()

    lines = [x.rstrip() for x in lines]
    random.shuffle(lines)

    fastText = fastTextDict

    fastTextSenteces = []
    classes = []

    for line in lines:
        wordAndValue = line.rsplit(',', 1)
        word = wordAndValue[0]
        value = int(wordAndValue[1])
        noPunchSentence = re.sub('[%s]' % re.escape(string.punctuation), '', word)
        sentenceList = noPunchSentence.split(" ")
        embeddedSentence = [fastTextDict.get(x) for x in sentenceList]
        embeddedSentence = [x for x in embeddedSentence if (np.array(x != None)).all()]
        if len(embeddedSentence) < MIN_WORDS:
            # print(word)
            continue
        if len(embeddedSentence) >= MAX_WORDS:
            x=np.random.choice(len(embeddedSentence), MAX_WORDS, False).astype(int)
            x.sort()
            embeddedSentence = np.array(embeddedSentence)[x]
        else:
            for _ in range(MAX_WORDS - len(embeddedSentence)):
                embeddedSentence.append([0] * 300)
        fastTextSenteces.append(embeddedSentence)
        classes.append(value)

    return fastTextSenteces, classes
