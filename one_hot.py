# one_hot.py
# sephi@class.me
# 03-08-2018

import numpy as np
import os
import re
import string

def oneHotFromHebrewSentence( sentence ):

    hebrewLetters = ['א','ב','ג','ד','ה','ו','ז','ח','ט','י','כ','ל','מ','נ','ס','ע','פ','צ','ק','ר','ש','ת','ם','ן','ץ','ף','ך']
    start = 27
    end = 28
    digit = 29
    latin_capital = 30
    latin_lower = 31
    punctuation = 32
    non_latin_or_hebrew = 33
    space = 34
    unexpected = 35
    lettersAsNumbers = [start]
    okSentence = 0
    for x in list(sentence):
        try:
            i = hebrewLetters.index(x)
            okSentence = 1
        except ValueError:
            if re.search('[0-9]', x):
                i = digit
            elif x == ' ':
                i = space
            elif re.search('[A-Z]', x):
                i = latin_capital
            elif re.search('[a-z]', x):
                i =latin_lower
            elif x in string.punctuation:
                i = punctuation
            elif x.isalpha():
                i = non_latin_or_hebrew
            else:
                 i = unexpected
            #i = len(hebrewLetters) + 1
            okSentence = 0
        lettersAsNumbers = np.append(lettersAsNumbers,[i])
    lettersAsNumbers = np.append(lettersAsNumbers,[end])
    lettersAsNumbers = np.append(lettersAsNumbers,[space] * (50 - len(lettersAsNumbers)))
    lettersAsNumbers = lettersAsNumbers[:50]
    n_values = 36
    return okSentence, (np.eye(n_values)[lettersAsNumbers.astype(int)])

def getLetterNum(x):
    hebrewLetters = ['א','ב','ג','ד','ה','ו','ז','ח','ט','י','כ','ל','מ','נ','ס','ע','פ','צ','ק','ר','ש','ת','ם','ן','ץ','ף','ך']
    start = 27
    end = 28
    digit = 29
    latin_capital = 30
    latin_lower = 31
    punctuation = 32
    non_latin_or_hebrew = 33
    space = 34
    unexpected = 35
    lettersAsNumbers = [start]
    okSentence = 0
    try:
        return hebrewLetters.index(x)
    except ValueError:
        if x == 'start':
            return start
        if x == 'end':
            return end
        if re.search('[0-9]', x):
            return digit
        elif x == ' ':
            return space
        elif re.search('[A-Z]', x):
            return latin_capital
        elif re.search('[a-z]', x):
            return latin_lower
        elif x in string.punctuation:
            return punctuation
        elif x.isalpha():
            return non_latin_or_hebrew
        else:
             return unexpected

def letterForNumber( num ):
    letters = ['א','ב','ג','ד','ה','ו','ז','ח','ט','י','כ','ל','מ','נ','ס','ע','פ','צ','ק','ר','ש','ת','ם','ן','ץ','ף','ך','<','>','0','A','a','!','π','_','@']
    return letters[num]

def twoLettersStatistics( num = 0 ):
    with open('wiki.he.vec') as f:
        lines = f.readlines()
    lines = [x.strip() for x in lines]
    print ("line length:", len(lines))
    coupleLettersArray = np.zeros((36, 36), dtype = int)
    for i, sentenceAndVec in enumerate(lines):
        sentence = sentenceAndVec.split(" ").pop(0)
        if (i % 1000 == 0):
            print (i)
        if num > 0 and i >= num:
            break
        for idx, letter in enumerate(sentence):
            if idx == 0:
                coupleLettersArray[getLetterNum('start')][getLetterNum(letter)] += 1
            if idx == (len(sentence) - 1):
                coupleLettersArray[getLetterNum(letter)][getLetterNum('end')] += 1
            else:
                coupleLettersArray[getLetterNum(letter)][getLetterNum(sentence[idx+1])] += 1

    for i, row in enumerate(coupleLettersArray):
        for j, count in enumerate(row):
            print (letterForNumber(i), letterForNumber(j), ":", count)


def oneHotListFromFile ( filename ):

    with open(filename) as f:
        lines = f.readlines()
    lines = [x.strip() for x in lines]
    arrayOfOneHotArryas = [oneHotFromHebrewSentence(x) for x in lines]
    return arrayOfOneHotArryas

#print (oneHotListFromFile("hebrew_senteces"))

def inputOtputVectorsFromFastTextData ( num = 0 ):
    with open('wiki.he.vec') as f:
        lines = f.readlines()
    lines = [x.strip() for x in lines]
    del lines[0] #just the number of rows
    i = 0
    wordsArray = []
    oneHotWordsArray = []
    fastTextVectorArray = []
    for line in lines:
        if (num > 0) and (i >= num):
            break
        i += 1
        wordAndVector = line.split(' ')
        word = wordAndVector.pop(0)
        ok, oneHotWord = oneHotFromHebrewSentence(word)
        if ok:
            fastTextVectorFloat = np.array(wordAndVector).astype(np.float)
            fastTextVectorArray.append(fastTextVectorFloat)
            oneHotWordsArray.append(oneHotWord)
    return (len(oneHotWordsArray), oneHotWordsArray, fastTextVectorArray)

def inputWordsFromFastTextOutputAsOffset ( num = 0 ):
    with open('data') as f:
        lines = f.readlines()
    lines = [x.strip() for x in lines]
    del lines[0] #just the number of rows
    wordsArray = []
    oneHotWordsArray = []
    oneHotOffsetArray = []
    for i, line in enumerate(lines):
        if (num > 0) and (i >= num): break
        word = line[:50]
        ok, oneHotWord = oneHotFromHebrewSentence(word)
        oneHotOffset = np.concatenate([oneHotWord[1:],np.eye(1,36,getLetterNum(' '),int)])
        oneHotOffsetArray.append(oneHotOffset)
        oneHotWordsArray.append(oneHotWord)
    return (len(oneHotWordsArray), oneHotWordsArray, oneHotOffsetArray)

def inputWordsFromXLOutputAsTag ( num = 0 ):
    with open('tagged_input') as f:
        lines = f.readlines()
    lines = [x.strip() for x in lines]
    wordsArray = []
    oneHotWordsArray = []
    oneHotOutputArray = []
    for i, line in enumerate(lines):
        if (num > 0) and (i >= num): break
        wordAndValue = line.rsplit(',', 1)
        word = wordAndValue[0][:50]
        value = wordAndValue[1]
        ok, oneHotWord = oneHotFromHebrewSentence(word)
        valueOneHot = np.zeros(36)
        valueOneHot[int(value)] = 1
        oneHotOutputArray.append(valueOneHot)
        oneHotWordsArray.append(oneHotWord)
    return (len(oneHotWordsArray), oneHotWordsArray, oneHotOutputArray)

def inputWordsFromDataOutputAsBackwardsAndOffset ( num = 0 ):
    with open('data') as f:
        lines = f.readlines()
    lines = [x.rstrip() for x in lines]
    del lines[0] #just the number of rows
    oneHotWordsArray = []
    oneHotWordsBackwards = []
    oneHotOffsetArray = []
    for i, line in enumerate(lines):
        if (num > 0) and (i >= num): break
        word = line[:50]
        if len(word.split(" ")) not in (3,4,5): continue
        _, oneHotWord = oneHotFromHebrewSentence(word)
        oneHotBackwards = oneHotWord[::-1]
        oneHotOffset = np.concatenate([oneHotWord[1:],np.eye(1,36,getLetterNum(' '),int)])
        oneHotOffsetArray.append(oneHotOffset)
        oneHotWordsBackwards.append(oneHotBackwards)
        oneHotWordsArray.append(oneHotWord)
    return (len(oneHotWordsArray), oneHotWordsBackwards, oneHotOffsetArray, oneHotWordsArray)

#inputVec, outputVec = inputOtputVectorsFromFastTextData() #you can limit number of lines
#twoLettersStatistics()
#batch, inputVec, outputVec = inputWordsFromFastTextOutputAsOffset(10)
# for x, y in zip(inputVec, outputVec):
#     print(x,y)
