import tensorflow as tf


class GRU_dec:

    def __init__(self, bridge_vector, hidden_size, dtype=tf.float64):
        self.input_dimensions = 192
        self.hidden_size = hidden_size
****    self.bridge_vector = bridge_vector

        # Weights for input vectors of shape (input_dimensions, hidden_size)
        self.Wr = tf.Variable(tf.truncated_normal(dtype=dtype, shape=(self.input_dimensions, self.hidden_size), mean=0, stddev=0.01), name='dec_Wr')
        self.Wz = tf.Variable(tf.truncated_normal(dtype=dtype, shape=(self.input_dimensions, self.hidden_size), mean=0, stddev=0.01), name='dec_Wz')
        self.Wh = tf.Variable(tf.truncated_normal(dtype=dtype, shape=(self.input_dimensions, self.hidden_size), mean=0, stddev=0.01), name='dec_Wh')

        # Weights for hidden vectors of shape (hidden_size, hidden_size)
        self.Ur = tf.Variable(tf.truncated_normal(dtype=dtype, shape=(self.hidden_size, self.hidden_size), mean=0, stddev=0.01), name='dec_Ur')
        self.Uz = tf.Variable(tf.truncated_normal(dtype=dtype, shape=(self.hidden_size, self.hidden_size), mean=0, stddev=0.01), name='dec_Uz')
        self.Uh = tf.Variable(tf.truncated_normal(dtype=dtype, shape=(self.hidden_size, self.hidden_size), mean=0, stddev=0.01), name='dec_Uh')

        # Biases for hidden vectors of shape (hidden_size,hidden_size)bridgeVector
        self.Cr = tf.Variable(tf.truncated_normal(dtype=dtype, shape=(self.hidden_size, self.hidden_size), mean=0, stddev=0.01), name='dec_Cr')
        self.Cz = tf.Variable(tf.truncated_normal(dtype=dtype, shape=(self.hidden_size, self.hidden_size), mean=0, stddev=0.01), name='dec_Cz')
        self.Ch = tf.Variable(tf.truncated_normal(dtype=dtype, shape=(self.hidden_size, self.hidden_size), mean=0, stddev=0.01), name='dec_Ch')
****
        # guessing the next bigram
        self.V  = tf.Variable(tf.truncated_normal(dtype=dtype, shape=(self.hidden_size, self.input_dimensions), mean=0, stddev=0.01), name='dec_V1')
        self.B  = tf.Variable(tf.truncated_normal(dtype=dtype, shape=(self.input_dimensions, ), mean=0, stddev=0.01), name='dec_B1')

        # Define the input layer placeholder
        self.input_layer = tf.Variable(tf.truncated_normal(dtype=dtype, shape=(500, 50, self.input_dimensions) ,mean=0, stddev=0.01), name='dec_input')

        # Put the time-dimension upfront for the scan operator
        self.x_t = tf.transpose(self.input_layer, [1, 0, 2], name='dec_x_t')

        # NO MORE NEEDED - a little hack (to obtain the same shape as the input matrix) to define the initial hidden state h_0
        # self.h_0 = tf.matmul(self.x_t[0, :, :], tf.zeros(dtype=tf.float64, shape=(self.input_dimensions, hidden_size)), name='dec_h_0')
        # print("***", self.h_0)
****
        embedded_zero_vector = tf.matmul(self.x_t[0, :, :], tf.zeros(dtype=tf.float64, shape=(self.input_dimensions, hidden_size)), name='embedded_zero_vector')   # given to initializer

        # Perform the scan operator
        self.h_t_transposed = tf.scan(self.forward_pass, elems = self.x_t, initializer = (self.bridge_vector, embedded_zero_vector), name='dec_h_t_transposed')[0]

        # Transpose the result back
        self.h_t = tf.transpose(self.h_t_transposed, [1,0,2], name='dec_h_t')

k-means clustring

    def forward_pass(self, two_vectors_from_previous_step, _in_decoder_this_to_be_ignored_):

        print (two_vectors_from_previous_step)
        h_tm1, x_t = two_vectors_from_previous_step

        # Definitions of gete vecctors z_t and r_t
        z_t = tf.sigmoid(tf.matmul(x_t, self.Wz) + tf.matmul(h_tm1, self.Uz) + tf.matmul(self.bridge_vector, self.Cz))
        r_t = tf.sigmoid(tf.matmul(x_t, self.Wr) + tf.matmul(h_tm1, self.Ur) + tf.matmul(self.bridge_vector, self.Cr))

        # Definition of h_proposal and h_t
        h_proposal = tf.tanh(
                               tf.matmul(x_t,                       self.Wh) +
                               tf.matmul(tf.multiply(r_t, h_tm1),   self.Uh) +
                               tf.matmul(self.bridge_vector,        self.Ch))

        # Compute the next hidden state
        h_t = tf.multiply(1 - z_t, h_tm1) + tf.multiply(z_t, h_proposal)

        guess_for_next_step = tf.matmul( h_t, self.V) + self.B

        return (h_t, guess_for_next_step)
