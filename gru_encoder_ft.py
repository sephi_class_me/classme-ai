import tensorflow as tf
import numpy as np
from gru_enc import GRU_enc
import gru_fast_text as data

# Generate a test set and a train set
X_train, Y_train = data.getFastTextSentecesAndClasses(["class0_train", "class1_train"])
Y_train = np.array([np.eye(3)[x] for x in Y_train])

print(len(X_train))

X_test, Y_test = data.getFastTextSentecesAndClasses(["class0_test", "class1_test"])
Y_test = np.array([np.eye(3)[x] for x in Y_test])

# print(len(X_train), Y_train.shape, len(X_test[0]), Y_test.shape)

input_dimensions = 300
output_dimensions = 3

# Arbitrary number for the size of the hidden state
hidden_size = int(np.power(24, 2)/3)

# Initialize a session
# session = tf.Session()
with tf.Session() as session:

    # Create a new instance of the GRU encoder model
    encoder = GRU_enc(input_dimensions, hidden_size)

    # Add an additional layer on top of each of the hidden state outputs
    W_output_enc = tf.Variable(tf.truncated_normal(dtype=tf.float64, shape=(hidden_size, 3), mean=0, stddev=0.01))
    b_output_enc = tf.Variable(tf.truncated_normal(dtype=tf.float64, shape=(3,), mean=0, stddev=0.01))
    output = tf.map_fn(lambda h_t: tf.matmul(h_t, W_output_enc) + b_output_enc, encoder.h_t)

    print (output.shape, encoder.h_t.shape, W_output_enc.shape, b_output_enc.shape, Y_train.shape)
    # Create a placeholder for the expected output
    expected_output = tf.placeholder(dtype=tf.float64, shape=Y_train.shape, name='expected_output')

    loss = tf.reduce_mean( tf.nn.softmax_cross_entropy_with_logits(logits=output, labels=expected_output) )
    # Use the Adam optimizer for training
    train_step = tf.train.AdamOptimizer(0.002).minimize(loss)

    # Initialize all the variables
    init_variables = tf.global_variables_initializer()
    session.run(init_variables)

    # Initialize the losses
    train_losses = []
    validation_losses = []

    # Perform all the iterations
    for epoch in range(20):
        # Compute the losses
        #print ("epoch ", epoch)
        _, train_loss = session.run([train_step, loss], feed_dict={encoder.input_layer: X_train, expected_output: Y_train})
        validation_loss, out = session.run([loss, output], feed_dict={encoder.input_layer: X_test, expected_output: Y_test})
        # Log the losses
        train_losses += [train_loss]
        validation_losses += [validation_loss]

        if epoch % 5 == 0:
            print('Iteration: %d, train loss: %.4f, test loss: %.4f' % (epoch, train_loss, validation_loss))
    # print(sentences[split_point])
    # [print (x) for x in Y_test[0]]
    # print (out[0])
    saver = tf.train.Saver()
    save_path = saver.save(session, "./models/model-gru-fastText")
    print("Model saved in path: %s" % save_path)
