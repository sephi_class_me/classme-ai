import numpy as np
import tensorflow as tf
import data_loader as dl
import one_hot as oh
import data_loader as dl
import random


# batch, inputVec, outputVec, lVec = dl.twoLettersSentence(100)
#
# split_point = int(batch / 2 )
#
# # Generate a test set and a train set
# X_train = np.array(inputVec[split_point:])
# Y_train = np.array(outputVec[split_point:])
# L_train = np.array(lVec[split_point:])
# X_test = np.array(inputVec[0:split_point])
# Y_test = np.array(outputVec[0:split_point])
# L_test = np.array(lVec[0:split_point])


def inputWordsFromXLOutputAsTag ( files ):
    # lines = []
    # for file in files:
    with open(files[0], encoding='utf-8') as f:
        lines = f.readlines()
    lines = [x.rstrip() for x in lines]
    random.shuffle(lines)
    embeddedVectorsArray = []
    oneHotClassArray = []
    for i, line in enumerate(lines):
        if (i >= 220): break
        wordAndValue = line.rsplit(',', 1)
        value = wordAndValue[1]
        embeddedSentence = dl.embeddedVector2grams(wordAndValue[0])
        valueOneHot = np.zeros(3)
        valueOneHot[int(value)] = 1
        embeddedVectorsArray.append(np.array(embeddedSentence))
        oneHotClassArray.append(valueOneHot)
        # print (line)
        # print (value)
        # print (np.argmax(valueOneHot))
    return ( embeddedVectorsArray, oneHotClassArray)

def thoughtVectorForEmbeddedVectors (embeddedVectorsArray):

    with tf.Session() as sess:
      # Restore variables from disk.
      saver = tf.train.import_meta_graph('./models/model-gru-nol.meta')
      saver.restore(sess, './models/model-gru-nol')
      graph = tf.get_default_graph()
      init_op = tf.global_variables_initializer()
      sess.run(init_op)
      bridgeVector = graph.get_tensor_by_name("bridge_vector:0")
      enc_input = graph.get_tensor_by_name("enc_input:0")
      # bridgeVectors = sess.run(bridgeVector, feed_dict={enc_input: X_test[1:2]})
      bridgeVectors1by1 = []
      for x in embeddedVectorsArray:
          bridge = sess.run(bridgeVector, feed_dict={enc_input: x.reshape([1,-1,196])})
          bridgeVectors1by1.append(bridge.reshape([-1]))

      return bridgeVectors1by1

def classifiedThoughtVectors():
    # train data
    embeddedVectorsTrain, train_classes = inputWordsFromXLOutputAsTag(['two_class.csv'])
    train_thought = thoughtVectorForEmbeddedVectors(embeddedVectorsTrain)
    # test data
    # embeddedVectorsTest, test_classes = inputWordsFromXLOutputAsTag(['class0_test', 'class1_test'])
    # test_thought = thoughtVectorForEmbeddedVectors(embeddedVectorsTest)
    return train_thought, train_classes

def thoughtVectorForSentence(sentence):

    embedded = dl.embeddedVector2grams(sentence)
    return thoughtVectorForEmbeddedVectors([np.array(embedded)])

def getRandomSentences(num = 200):
    with open('data', encoding='utf-8') as f:
        lines = f.readlines()
    lines = [x.strip() for x in lines]
    random.shuffle(lines)
    with open('tagged_input', encoding='utf-8') as f:
        tagged = f.readlines()
    tagged = [x.strip().rsplit(',', 1)[0] for x in tagged]
    i = 0
    print (len(tagged))
    randomSentences = []
    for line in lines:
        if (i > num): break
        if line not in tagged:
            randomSentences.append(line)
            print (i)
            i+=1
        else: print (lines[i])

    [print(x + ",5") for x in randomSentences]
