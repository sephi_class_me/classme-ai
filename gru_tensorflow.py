#%% (0) Important libraries
import tensorflow as tf
import numpy as np
import data_loader as dl
from gru_dec import GRU_dec
from gru_enc import GRU_enc


batch, inputVec, outputVec, sentences = dl.twoLettersSentence(1000)

split_point = int(batch / 2 )

# Generate a test set and a train set
X_train = np.array(inputVec[0:split_point])
Y_train = np.array(outputVec[0:split_point])

X_test = np.array(inputVec[split_point:])
Y_test = np.array(outputVec[split_point:])

input_dimensions = 196
output_dimensions = 196

# Arbitrary number for the size of the hidden state
hidden_size = int(np.power(24, 2)/3)

# Initialize a session
# session = tf.Session()
with tf.Session() as session:

    # Create a new instance of the GRU encoder model
    encoder = GRU_enc(input_dimensions, hidden_size)

    # Add an additional layer on top of each of the hidden state outputs
    #W_output_enc = tf.Variable(tf.truncated_normal(dtype=tf.float64, shape=(hidden_size, 36), mean=0, stddev=0.01))
    #b_output_enc = tf.Variable(tf.truncated_normal(dtype=tf.float64, shape=(36,), mean=0, stddev=0.01))
    #tf.map_fn(lambda h_t: tf.matmul(tf.reshape(h_t, [1, -1]), W_output_enc) + b_output_enc, tf.gather(encoder.h_t_transposed, 49))
    bridgeVector = tf.gather(encoder.h_t_transposed, 49, name="bridge_vector")

    decoder = GRU_dec(bridgeVector, hidden_size)
    W_output_dec = tf.Variable(tf.truncated_normal(dtype=tf.float64, shape=(hidden_size, output_dimensions), mean=0, stddev=0.01))
    b_output_dec = tf.Variable(tf.truncated_normal(dtype=tf.float64, shape=(output_dimensions,), mean=0, stddev=0.01))
    output =  tf.map_fn(lambda h_t: tf.matmul(h_t, W_output_dec) + b_output_dec, decoder.h_t, name="output")
    print (output.shape, decoder.h_t.shape, W_output_dec.shape, b_output_dec.shape, Y_train.shape)
    # Create a placeholder for the expected output
    expected_output = tf.placeholder(dtype=tf.float64, shape=Y_train.shape, name='expected_output')

    # Just use quadratic loss
    loss = tf.reduce_sum(0.5 * tf.pow(output - expected_output, 2)) / float(split_point)
    # loss = tf.reduce_mean(-tf.reduce_sum(expected_output * tf.log(output), reduction_indices=[1]))
    # loss_test = tf.reduce_sum(0.5 * tf.pow(output - expected_output, 2)) / float()
    # Use the Adam optimizer for training
    train_step = tf.train.AdamOptimizer(0.002).minimize(loss)

    # Initialize all the variables
    init_variables = tf.global_variables_initializer()
    session.run(init_variables)

    # Initialize the losses
    train_losses = []
    validation_losses = []

    # Perform all the iterations
    for epoch in range(20):
        # Compute the losses
        #print ("epoch ", epoch)
        _, train_loss = session.run([train_step, loss], feed_dict={encoder.input_layer: X_train, expected_output: Y_train})
        validation_loss, out = session.run([loss, output], feed_dict={encoder.input_layer: X_test, expected_output: Y_test})
        # Log the losses
        train_losses += [train_loss]
        validation_losses += [validation_loss]

        if epoch % 5 == 0:
            print('Iteration: %d, train loss: %.4f, test loss: %.4f' % (epoch, train_loss, validation_loss))
    # print(sentences[split_point])
    # [print (x) for x in Y_test[0]]
    # print (out[0])
    saver = tf.train.Saver()
    save_path = saver.save(session, "./models/model-gru-oh")
    print("Model saved in path: %s" % save_path)
