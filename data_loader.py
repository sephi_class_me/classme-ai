import numpy as np
import tensorflow as tf
import one_hot as oh
import random
import re
import string

#load conversion matrix
with tf.Session() as sess:
  # Restore variables from disk.
  saver = tf.train.import_meta_graph('./models/model-emb_vec-196-3000.meta')
  saver.restore(sess,'./models/model-emb_vec-196-3000')
  # Check the values of the variables
  graph = tf.get_default_graph()
  W1 = graph.get_tensor_by_name("W1:0").eval()


def embeddedVectorForTwoLetters(first,second):
    return W1[getLetterNum(second) *  36 + getLetterNum(first)]


def getLetterNum(x):
    hebrewLetters = ['א','ב','ג','ד','ה','ו','ז','ח','ט','י','כ','ל','מ','נ','ס','ע','פ','צ','ק','ר','ש','ת','ם','ן','ץ','ף','ך']
    start = 27
    end = 28
    digit = 29
    latin_capital = 30
    latin_lower = 31
    punctuation = 32
    non_latin_or_hebrew = 33
    space = 34
    unexpected = 35
    lettersAsNumbers = [start]
    okSentence = 0
    try:
        return hebrewLetters.index(x)
    except ValueError:
        if x == 'start':
            return start
        if x == 'end':
            return end
        if re.search('[0-9]', x):
            return digit
        elif x == ' ':
            return space
        elif x == 'space':
            return space
        elif re.search('[A-Z]', x):
            return latin_capital
        elif re.search('[a-z]', x):
            return latin_lower
        elif x in string.punctuation:
            return punctuation
        elif x.isalpha():
            return non_latin_or_hebrew
        else:
             return unexpected

def embeddedVector2grams(sentence):
    embeddedSentence = []
    for idx, letter in enumerate(sentence):
        if idx == 0:
            embeddedSentence.append(embeddedVectorForTwoLetters('start', 'start'))
            embeddedSentence.append(embeddedVectorForTwoLetters('start', letter))
        if idx == (len(sentence) - 1):
            vector = embeddedVectorForTwoLetters(letter, 'end')
        else:
            vector = embeddedVectorForTwoLetters(letter, sentence[idx+1])
        embeddedSentence.append(vector)

    embeddedSentence.append(embeddedVectorForTwoLetters('end', ' '))
    for i in range(50 - len(embeddedSentence)):
        embeddedSentence.append(embeddedVectorForTwoLetters(' ', ' '))

    return embeddedSentence[:50]

def oneHotFromHebrewSentenceOffset( sentence ):
    lettersAsNumbers = [getLetterNum('start')]
    for x in list(sentence):
        lettersAsNumbers = np.append(lettersAsNumbers,[getLetterNum(x)])
    lettersAsNumbers = np.append(lettersAsNumbers,[getLetterNum('end')])
    lettersAsNumbers = np.append(lettersAsNumbers,[getLetterNum('space')] * (51 - len(lettersAsNumbers)))
    lettersAsNumbers = lettersAsNumbers[1:51]
    n_values = 36
    return (np.eye(n_values)[lettersAsNumbers.astype(int)])


def twoLettersEmbeddedOneLetterOneHot( num = 0 ):
    with open('data', encoding='utf-8') as f:
        lines = f.readlines()
    lines = [x.rstrip() for x in lines]
    random.shuffle(lines)
    print ("line length:", len(lines))
    sentences = []
    embeddedSentenceArray = []
    oneHotSentencesArray = []
    for i, sentence in enumerate(lines):
        if (i % 1000 == 0):
            print (i)
        if num > 0 and i >= num:
            break
        sentences.append(sentence)
        embeddedSentenceArray.append(embeddedVector2grams(sentence)[::-1])
        oneHotSentencesArray.append(oneHotFromHebrewSentenceOffset(sentence))

    return len(embeddedSentenceArray), embeddedSentenceArray, oneHotSentencesArray, sentences

def twoLettersSentence( num = 0 ):
    with open('data', encoding='utf-8') as f:
        lines = f.readlines()
    lines = [x.rstrip() for x in lines]
    random.shuffle(lines)
    print ("line length:", len(lines))
    embeddedSentenceArray = []
    embeddedSentenceBackwardArray = []
    embeddedSentenceOffsetArray = []
    for i, sentenceAndVec in enumerate(lines):
        sentence = sentenceAndVec.split(" ").pop(0)
        if (i % 1000 == 0):
            print (i)
        if num > 0 and i >= num:
            break
        embeddedSentence = []
        for idx, letter in enumerate(sentence):
            if idx == 0:
                embeddedSentence.append(embeddedVectorForTwoLetters('start', 'start'))
                embeddedSentence.append(embeddedVectorForTwoLetters('start', letter))
            if idx == (len(sentence) - 1):
                vector1 = embeddedVectorForTwoLetters(letter, 'end')
            else:
                vector1 = embeddedVectorForTwoLetters(letter, sentence[idx+1])
            embeddedSentence.append(vector1)

        embeddedSentence.append(embeddedVectorForTwoLetters('end', ' '))
        for i in range(50 - len(embeddedSentence)):
            embeddedSentence.append(embeddedVectorForTwoLetters(' ', ' '))
        embeddedSentence = embeddedSentence[:50]
        embeddedSentenceNP = np.array(embeddedSentence)
        embeddedSentenceBackward = embeddedSentenceNP[::-1]
        embeddedSentence.append(embeddedVectorForTwoLetters(' ', ' '))
        embeddedSentenceOffset = np.array(embeddedSentence[1:])
        embeddedSentenceArray.append(embeddedSentenceNP)
        embeddedSentenceBackwardArray.append(embeddedSentenceBackward)
        embeddedSentenceOffsetArray.append(embeddedSentenceOffset)

    return len(embeddedSentenceArray), embeddedSentenceArray, embeddedSentenceOffsetArray, embeddedSentenceBackwardArray


# def twoLettersSentence( num = 0 ):
#     with open('data', encoding='utf-8') as f:
#         lines = f.readlines()
#     lines = [x.rstrip() for x in lines]
#     random.shuffle(lines)
#     print ("line length:", len(lines))
#     embeddedSentenceArray = []
#     embeddedSentenceBackwardArray = []
#     embeddedSentenceOffsetArray = []
#     for i, sentenceAndVec in enumerate(lines):
#         sentence = sentenceAndVec.split(" ").pop(0)
#         if (i % 1000 == 0):
#             print (i)
#         if num > 0 and i >= num:
#             break
#         embeddedSentence = []
#         for idx, letter in enumerate(sentence):
#             if idx == 0:
#                 vector = embeddedVectorForTwoLetters('start', letter)
#                 embeddedSentence.append(vector)
#             if idx == (len(sentence) - 1):
#                 vector = embeddedVectorForTwoLetters(letter, 'end')
#             else:
#                 vector = embeddedVectorForTwoLetters(letter, sentence[idx+1])
#             embeddedSentence.append(vector)
#
#         embeddedSentence.append(embeddedVectorForTwoLetters('end', ' '))
#         for i in range(50 - len(embeddedSentence)):
#             embeddedSentence.append(embeddedVectorForTwoLetters(' ', ' '))
#         embeddedSentence = embeddedSentence[:50]
#         embeddedSentenceNP = np.array(embeddedSentence)
#         embeddedSentenceBackward = embeddedSentenceNP[::-1]
#         embeddedSentence.append(embeddedVectorForTwoLetters(' ', ' '))
#         embeddedSentenceOffset = np.array(embeddedSentence[1:])
#         embeddedSentenceArray.append(embeddedSentenceNP)
#         embeddedSentenceBackwardArray.append(embeddedSentenceBackward)
#         embeddedSentenceOffsetArray.append(embeddedSentenceOffset)
#
#     return len(embeddedSentenceBackwardArray), embeddedSentenceBackwardArray, embeddedSentenceOffsetArray, embeddedSentenceArray
