import numpy as np
import tensorflow as tf
import random
from tensorflow.contrib.factorization.python.ops import clustering_ops
from tensorflow.python.estimator import estimator
from tensorflow.python.estimator import model_fn as model_fn_lib
from tensorflow.python.estimator.export import export_output
from tensorflow.python.feature_column import feature_column as fc
from tensorflow.python.framework import ops
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import control_flow_ops
from tensorflow.python.ops import math_ops
from tensorflow.python.ops import metrics
from tensorflow.python.ops import state_ops
from tensorflow.python.platform import tf_logging as logging
from tensorflow.python.saved_model import signature_constants
from tensorflow.python.summary import summary
from tensorflow.python.training import session_run_hook
from tensorflow.python.training import training_util




num = 1000
num_clusters = 150

with open('wiki.he.vec', encoding='utf-8') as f:
    lines = f.readlines()
lines = [x.rstrip() for x in lines]
del lines[0]
random.shuffle(lines)
vectorsArray = []
for i, wordAndVec in enumerate(lines):
    if (i % 10000 == 0): print (i)
    if num > 0 and i >= num: break
    vec = wordAndVec.split(" ")
    del(vec[0])
    if len(vec) != 300:
        print (i)
        continue
    vectorsArray.append(vec)

points = np.array(vectorsArray)

centers = tf.train.load_variable(
    "./models/kmeans11k", KMeansClustering.CLUSTER_CENTERS_VAR_NAME)

kmeans = tf.contrib.factorization.KMeansClustering( model_dir="./models/kmeans11k", initial_clusters=centers)
# predict_fn = tf.contrib.predictor.from_saved_model("./models/kmeans1k/1523875308")
print (kmeans.cluster_centers())
print (center)

quit()

def serving_input_receiver_fn():
  inputs = {"x": tf.placeholder(shape=[None, 300], dtype=tf.float32)}
  return tf.estimator.export.ServingInputReceiver(inputs, inputs)

def input_fn():
  return tf.train.limit_epochs(
      tf.convert_to_tensor(points, dtype=tf.float32), num_epochs=1)


kmeans = tf.contrib.factorization.KMeansClustering(
    num_clusters=num_clusters, model_dir="./models/kmeans11k/" , use_mini_batch=False)

# train
num_iterations = 20
previous_centers = None
for _ in range(num_iterations):
  kmeans.train(input_fn)
  cluster_centers = kmeans.cluster_centers()
  if previous_centers is not None:
    print ('delta:', cluster_centers - previous_centers)
  previous_centers = cluster_centers
  print ('score:', kmeans.score(input_fn)/num)
print ('cluster centers:', np.mean(cluster_centers))

# map the input points to their clusters
cluster_indices = list(kmeans.predict_cluster_index(input_fn))
for i, point in enumerate(points):
  cluster_index = cluster_indices[i]
  center = cluster_centers[cluster_index]
  print ('point:', point[0], 'is in cluster', cluster_index, 'centered at', np.mean(center))
