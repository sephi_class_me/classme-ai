import tensorflow as tf
import numpy as np
import one_hot as oh
import random

def twoLettersStatistics( num = 0 ):
    with open('wiki.he.vec', encoding='utf-8') as f:
        lines = f.readlines()
    lines = [x.rstrip() for x in lines]
    del lines[0]
    random.shuffle(lines)
    inputArray = []
    outputArray = []
    words = []
    orig = []
    for i, wordAndVec in enumerate(lines):
        if (i % 10000 == 0): print (i)
        if num > 0 and i >= num: break
        vec = wordAndVec.split(" ")
        word = vec.pop(0)
        if len(vec) != 300:
            print (i, word)
            continue
        embeddedVectorFloat = np.array(vec).astype(np.float)
        coupleLettersArray = np.zeros((36, 36))
        for idx, letter in enumerate(word):
            if idx == 0:
                coupleLettersArray[oh.getLetterNum('start')][oh.getLetterNum(letter)] += 0.025
                coupleLettersArray[oh.getLetterNum('space')][oh.getLetterNum(letter)] += 0.025
            if idx == (len(word) - 1):
                coupleLettersArray[oh.getLetterNum(letter)][oh.getLetterNum('end')] += 0.025
                coupleLettersArray[oh.getLetterNum(letter)][oh.getLetterNum('space')] += 0.025
            else:
                coupleLettersArray[oh.getLetterNum(letter)][oh.getLetterNum(word[idx+1])] += 1
            # if idx != (len(word) - 1):
            #     coupleLettersArray[oh.getLetterNum(letter)][oh.getLetterNum(word[idx+1])] += 1
        words.append(word)
        orig.append(coupleLettersArray)
        coupleLettersArray = coupleLettersArray.flatten()
        coupleLettersArray /= coupleLettersArray.sum()
        inputArray.append(coupleLettersArray)
        outputArray.append(embeddedVectorFloat)

    return words, inputArray, outputArray

words, input, output = twoLettersStatistics(1000) # 2573 250

ONE_HOT_VECTOR_SIZE = 1296
EMBEDDED_VECTOR_SIZE = 196
SEMANTIC_VECTOR_SIZE = 576
FASTTEXT_VECTOR_SIZE = 300

x = tf.placeholder(shape=[None, ONE_HOT_VECTOR_SIZE], dtype=tf.float32)
y = tf.placeholder(shape=[None, FASTTEXT_VECTOR_SIZE], dtype=tf.float32)

W1 = tf.Variable(   initial_value = tf.truncated_normal(
                        dtype=tf.float32,
                        shape=(ONE_HOT_VECTOR_SIZE, EMBEDDED_VECTOR_SIZE),
                        mean=0,
                        stddev=0.1,
                    ),
                    name='W1')

# b1 = tf.Variable(   initial_value = tf.truncated_normal(
#                         dtype=tf.float32,
#                         shape=(EMBEDDED_VECTOR_SIZE,),
#                         mean=0,
#                         stddev=0.1,
#                     ),
#                     name='b1')


intermediate_1 = tf.matmul(x, W1)


W2 = tf.Variable(tf.truncated_normal(dtype=tf.float32,
                                     shape=(EMBEDDED_VECTOR_SIZE, SEMANTIC_VECTOR_SIZE),
                                     mean=0,
                                     stddev=0.1),
                      name='W2')

b2 = tf.Variable(tf.truncated_normal(dtype=tf.float32,
                                     shape=(SEMANTIC_VECTOR_SIZE,),
                                     mean=0,
                                     stddev=0.1),
                      name='b2')


intermediate_2 = tf.tanh(tf.matmul(intermediate_1, W2) + b2)

W3 = tf.Variable(tf.truncated_normal(dtype=tf.float32,
                                          shape=(SEMANTIC_VECTOR_SIZE, FASTTEXT_VECTOR_SIZE),
                                          mean=0,
                                          stddev=0.25),
                      name='W3')

b3 = tf.Variable(tf.truncated_normal(dtype=tf.float32,
                                          shape=(FASTTEXT_VECTOR_SIZE,),
                                          mean=0,
                                          stddev=0.25),
                      name='b3')


result = tf.matmul(intermediate_2, W3) + b3

cost = tf.reduce_mean((result - y)**2)
optimizer = tf.train.AdamOptimizer(0.002).minimize(cost)
init = tf.global_variables_initializer()

split_point = int(len(words) / 5 )

x_vals_train = np.array(input [split_point:])     # the average of one-hot vectors for each word
y_vals_train = np.array(output [split_point:])    # the corresponding FastText vectors fo each word

x_vals_test = np.array(input [0:split_point])        # the average of one-hot vectors for each word
y_vals_test = np.array(output) [0:split_point]       # the corresponding FastText vectors fo each word

with tf.Session() as sess:
    sess.run(init)

    for step in range(10):
        _, val1 = sess.run([optimizer, cost],
                                   feed_dict={ x: x_vals_train, y: y_vals_train })

        validation_loss = sess.run(cost,
                                   feed_dict={ x: x_vals_test, y: y_vals_test })


        if step % 5 == 0:
            print("step: {}, train: {}, test: {}".format(step, val1, validation_loss))

    # from datetime import datetime as dt
    # time = dt.now().strftime("%Y%m%d%H%M%S")
    # saver = tf.train.Saver()
    # save_path = saver.save(sess, "./models/model-emb_vec-196")
    # print("Model saved in path: %s" % save_path)
