import tensorflow as tf
import numpy as np
import gru_loader as gl
import random


def addGausianNoise(vec):
    RAND_ITER = 5
    newVec = []
    for x in vec:
        num = 0
        for i in range(RAND_ITER):
             num += random.uniform(0, 1)
        num /= RAND_ITER
        num -= 0.5
        num *= float(1.0/1000.0)
        x += num
        newVec.append(x)
    return np.array(newVec)



# x_vals_train, y_vals_train, x_vals_test, y_vals_test = gl.classifiedThoughtVectors()
# x_vals_train = [addGausianNoise(vec) for vec in x_vals_train]
# x_vals_test = [addGausianNoise(vec) for vec in x_vals_test]
input, output = gl.classifiedThoughtVectors()
input = [addGausianNoise(vec) for vec in input]

MESSAGE_VECTOR_SIZE = 192
BOTTLENECK_VECTOR_SIZE = 75
RELAX_VECTOR_SIZE = 350
INTERMEDIATE_VECTOR_SIZE = 250
SIGNAL_VECTOR_SIZE = 3

x = tf.placeholder(shape=[None, MESSAGE_VECTOR_SIZE], dtype=tf.float32)
y = tf.placeholder(shape=[None, SIGNAL_VECTOR_SIZE], dtype=tf.float32)

W1 = tf.Variable(   initial_value = tf.truncated_normal(
                        dtype=tf.float32,
                        shape=(MESSAGE_VECTOR_SIZE, BOTTLENECK_VECTOR_SIZE),
                        mean=0,
                        stddev=0.1,
                    ),
                    name='W1')

b1 = tf.Variable(   initial_value = tf.truncated_normal(
                        dtype=tf.float32,
                        shape=(BOTTLENECK_VECTOR_SIZE,),
                        mean=0,
                        stddev=0.1,
                    ),
                    name='b1')


intermediate_1 = tf.tanh(tf.matmul(x, W1) + b1)

W2 = tf.Variable(tf.truncated_normal(dtype=tf.float32,
                                     shape=(BOTTLENECK_VECTOR_SIZE, RELAX_VECTOR_SIZE),
                                     mean=0,
                                     stddev=0.1),
                      name='W2')

b2 = tf.Variable(tf.truncated_normal(dtype=tf.float32,
                                     shape=(RELAX_VECTOR_SIZE,),
                                     mean=0,
                                     stddev=0.1),
                      name='b2')

intermediate_2 = tf.tanh(tf.matmul(intermediate_1, W2) + b2)

W3 = tf.Variable(tf.truncated_normal(dtype=tf.float32,
                                          shape=(RELAX_VECTOR_SIZE, INTERMEDIATE_VECTOR_SIZE),
                                          mean=0,
                                          stddev=0.25),
                      name='W3')

b3 = tf.Variable(tf.truncated_normal(dtype=tf.float32,
                                          shape=(INTERMEDIATE_VECTOR_SIZE,),
                                          mean=0,
                                          stddev=0.25),
                      name='b3')

intermediate_3 = tf.tanh(tf.matmul(intermediate_2, W3) + b3)


W4 = tf.Variable(tf.truncated_normal(dtype=tf.float32,
                                          shape=(INTERMEDIATE_VECTOR_SIZE, SIGNAL_VECTOR_SIZE),
                                          mean=0,
                                          stddev=0.25),
                      name='W4')

b4 = tf.Variable(tf.truncated_normal(dtype=tf.float32,
                                          shape=(SIGNAL_VECTOR_SIZE,),
                                          mean=0,
                                          stddev=0.25),
                      name='b4')

intermediate_4 = tf.matmul(intermediate_3, W4) + b4

result = tf.nn.softmax (intermediate_4)


cost = tf.reduce_mean(-tf.reduce_sum(y * tf.log(result), reduction_indices=[1]))
optimizer = tf.train.GradientDescentOptimizer(0.005).minimize(cost)

# cost = tf.reduce_mean((result - y)**2)
# optimizer = tf.train.AdamOptimizer(0.0002).minimize(cost)
init = tf.global_variables_initializer()

split_point = int(len(input)/ 5)

x_vals_train = np.array(input [split_point:])
y_vals_train = np.array(output [split_point:])

x_vals_test = np.array(input [0:split_point])
y_vals_test = np.array(output) [0:split_point]

with tf.Session() as sess:
    sess.run(init)

    for step in range(10000):
        _, val1 = sess.run([optimizer, cost],
                                   feed_dict={ x: x_vals_train, y: y_vals_train })

        validation_loss = sess.run(cost,
                                   feed_dict={ x: x_vals_test, y: y_vals_test })
        if step % 100 == 0:
            print("step: {}, train: {}, test: {}".format(step, val1, validation_loss))

    # sent = np.array(gl.thoughtVectorForSentence("ימכוערת בפנים"))
    # vec = result.eval({x: sent})
    # # vec = addGausianNoise(vec)
    # print (vec.argmax(), vec)
    total = 0
    good = 0
    print(x_vals_test[0].shape)
    for x_, y_ in zip(x_vals_test, y_vals_test):
        vec = result.eval({x:x_.reshape(1,-1)})
        # rand = random.uniform(0, 1)
        # vec = np.array([rand, 1-rand])
        res = vec.argmax()
        total += 1
        good += int(res == y_.argmax())
        print (res, res == y_.argmax())
    print(float(good/total))
